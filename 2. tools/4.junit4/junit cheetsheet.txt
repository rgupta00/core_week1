
juint basics
_____________

Hello World calculator maven project

junit annotations:
__________________
	@BeforeClass:
		Run only 1 time before all the test cases of class. 
		Can be used to initialize resources.

	@AfterClass :
		Run only 1 time after all the test cases of class. 
		Can be used to clean up the resources.

	@Before :
		Run annotated method every time individual test case is 
		executed but run before the test case.

	@After :
		Run annotated method every time individual test case is 
		executed but run after the test case


Assertion methods
___________________
      assertEquals():-
      fail() :- fail a test with no reason
      assertNull() :- Asserts that object is null.
      assertNotNull() :- Asserts that object is not null.
      assertTrue() :- Asserts that condition is true.
      assertFalse() : Asserts that condition is false.


Exception Checking Using Junit
_______________________


Ignore Test Cases
__________________
Sometimes we want to run only few test cases then we can ignore 
few test cases with @Ignore annotation. 

How to test with multiple parameters



more on junit
____________
	Ignore
	exception
	timeout


Test suite
____________
	@RunWith(Suite.class)
	@SuiteClasses({A.class, B.class}
	public class TestAll{

	}



parameterized unit test cases*
_________________________
Need of parameterized unit test case?

Step 1: create an test class (CalculatorTest)

 with annotation @RunWith
@RunWith(Parameterized.class)



step 2: create an static method with @Parameters


	@Parameters
	public static Collection<Object[]> testData(){
		Object[][]data=new Object[][]{{4,2,2},{4,1,3}};
		return Arrays.asList(data);
	}



step 3: put as instance variable

	private int expectedResult;
	private int firstNumber;
	private int secondNumber;


	
	Calculator calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator=new Calculator();
	}
	
	public CalculatorTest(int expectedResult, int firstNumber, int secondNumber) {
		super();
		this.expectedResult = expectedResult;
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
	}
	
	

	@Test
	public void testAdd(){
		Assert.assertEquals(expectedResult, calculator.add(firstNumber, secondNumber));
	}
	@After
	public void tearDown() throws Exception {
		calculator=null;
	}



crud application
_______________


import java.util.Date;
public class Event
{
    private int id;
    private String title;
	//getter setter
}




public interface EventDAO
{
    int saveEvent( Event event );
    Event getEvent( int id );
    void deleteEvent( int id );
}



public class MemoryEventDAO implements EventDAO
{
    private Map<Integer, Event> events;
    
    private int identifier;
    
    public MemoryEventDAO()
    {
        events = new HashMap<Integer, Event>();
    }
    
    public int saveEvent( Event event )
    {
        event.setId( ++identifier );
   
        events.put( identifier, event );
        
        return identifier;
    }
    
    public Event getEvent( int id )
    {
        return events.get( id );
    }
    
    public void deleteEvent( int id )
    {
        events.remove( id );
    }
}


jUnit 4.x
----------------


public class EventDAOAnnotatedTest
{
    private EventDAO eventDAO;
    
    private Event event;
    
    @Before
    public void invokedBeforeTests() // Sets up the test fixture
    {
        eventDAO = new MemoryEventDAO(); // Instantiating a new EventDAO for each test
        
        
        event = new Event( "U2 concert"); // Creating a convenient Event instance
    }
    
    @Test
    public void addEvent()
    {
        int id = eventDAO.saveEvent( event );
        
        event = eventDAO.getEvent( id );
        
        assertEquals( id, event.getId() ); // Verify equality between the added and the retrieved object
        assertEquals( "U2 concert", event.getTitle() );        
       
    }

    @Test
    public void getEvent()
    {
        int id = eventDAO.saveEvent( event );

        event = eventDAO.getEvent( id );
        
        assertNotNull( event );
        
        assertEquals( id, event.getId() );        
        assertEquals( "U2 concert", event.getTitle() );        
        
        event = eventDAO.getEvent( -1 );
        
        assertNull( event ); // Verify that null is returned when object does not exist               
    }

    @Test
    public void deleteEvent()
    {
        int id = eventDAO.saveEvent( event );
        
        event = eventDAO.getEvent( id );
        
        assertNotNull( event ); // Verify that event exists
        
        eventDAO.deleteEvent( id );
        
        event = eventDAO.getEvent( id );
        
        assertNull( event ); // Verify that event has been deleted
    }
    
    @After
    public void invokedAfterTests()
    {
        // Do some useful tear-down work
    }
}





junit mock testing:
______________

Basic concepts:
__________

Unit testing:
____________
	where any given test covers the smallest unit of a program (a function or procedure).
	 It may or may not take some input parameters and may or may not return some values.

Integration testing:
________________
	 where individual units are tested together to check whether all the
	 units interact with each other as expected.

What is Mocking and When Does It Come into the Picture?
___________________________________________________

	What if we want to test service layer without completion of dao layer?
	What if we want to test one service which depend on an service available on other application?

	
	=> mockito used for mocking


getting started:
________________-
public interface BookDao {
	public List<String> getBooks();
}

public class BookDaoImpl implements BookDao {
	@Override
	public List<String> getBooks() {
		return Arrays.asList("java","rich dad poor dad","java adv");
	}
}

public interface BookService {
	public List<String> getBooks(String subject);
}


public class BookServiceImpl implements BookService {
	private BookDao bookDao;

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public List<String> getBooks(String subject) {
		return bookDao.getBooks().stream().filter(title -> title.contains(subject)).collect(Collectors.toList());
	}

}




import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.bookapp.dao.BookDao;
import com.bookapp.dao.BookDaoImpl;

public class BookServiceTest {
	@Test
	public void getBookTest() {
		BookDao dao=new BookDaoImpl();
		
		BookServiceImpl bookService=new BookServiceImpl();
		bookService.setBookDao(dao);
		List<String> books=bookService.getBooks("java");
		Assertions.assertEquals(2, books.size());
	}
}




Useful Snippets and References
Easier Static Imports

Window > Preferences > Java > Editor > Content Assist > Favorites

import org.junit.jupiter.api.Assertions
org.mockito.BDDMockito
org.mockito.Mockito
org.assertj.core.api.Assertions
org.hamcrest.Matchers
org.hamcrest.CoreMatchers
org.hamcrest.MatcherAssert


mockito hello world:
----------------------

public class DemoTest {

	@Test
	public void getBookTest() {
		
		BookDao dao=mock(BookDao.class);
		
		List<String> allbooks=Arrays.asList("java","rich dad poor dad","java adv");
		
		when(dao.getBooks()).thenReturn(allbooks);
		
		BookServiceImpl bookService=new BookServiceImpl();
		bookService.setBookDao(dao);
		List<String> books=bookService.getBooks("java");
		Assert.assertEquals(2, books.size());
	}
}


Improvement 1:
________________

public class DemoTest2 {

	BookDao dao = mock(BookDao.class);
	
	BookServiceImpl bookService = new BookServiceImpl();

	@Before
	public void before() {
		List<String> allbooks = Arrays.asList("java", "rich dad poor dad", "java adv");

		when(dao.getBooks()).thenReturn(allbooks);
		BookDao dao = mock(BookDao.class);
	}

	@Test
	public void getBookTest() {

		bookService.setBookDao(dao);
		List<String> books = bookService.getBooks("java");
		Assert.assertEquals(2, books.size());
	}
}



mockito with annotations:
___________________________-

@RunWith(MockitoJUnitRunner.class)
public class DemoTest2 {

	@InjectMocks
	BookServiceImpl bookService = new BookServiceImpl(); //even we dont need to create object

	
	@Mock
	BookDao dao;
	

	@Before
	public void before() {
		List<String> allbooks = Arrays.asList("java", "rich dad poor dad", "java adv");

		when(dao.getBooks()).thenReturn(allbooks);
		
	}

	@Test
	public void getBookTest() {

		bookService.setBookDao(dao);
		List<String> books = bookService.getBooks("java");
		Assert.assertEquals(2, books.size());
	}
}



MockitoAnnotations.initMocks(this) vs @RunWith(MockitoJUnitRunner.class)

Note:
________
Mockito cannot mock or spy on Java constructs such as final classes and
methods, static methods, enums, private methods, the equals() and
hashCode() methods, primitive types, and anonymous classes


Partial Mocking: @Spy
______________________
  When we want an object in the test class to mock some method(s),
 but also call some actual method(s), then we need partial mocking. 
  This is achieved via @Spy

	Unlike using @Mock, with @Spy, a real object is created, 
	but the methods of that object can be mocked or can be actually called—whatever we need.



Example:
____________
@Repository
public class BookDaoImpl implements BookDao {
	@Override
	public List<String> getBooks() {
		log();
		return null;
	}

	public void log() {
		System.out.println("----------");
	}
}



@RunWith(MockitoJUnitRunner.class)
public class DemoTest {

	@Spy
	BookDaoImpl dao;
	
	@InjectMocks
	BookServiceImpl bookService;

	// when tested log() method is going to be called.....

}


A few mockito examples mocking List class
__________________________________________

public class ListTest {

	@Test
	public void letsMockListSize() {
		List list = mock(List.class);
		Mockito.when(list.size()).thenReturn(10);
		assertEquals(10, list.size());
	}

	@Test
	public void letsMockListSizeWithMultipleReturnValues() {
		List list = mock(List.class);
		Mockito.when(list.size()).thenReturn(10).thenReturn(20);
		assertEquals(10, list.size()); // First Call
		assertEquals(20, list.size()); // Second Call
	}

	@Test
	public void letsMockListGet() {
		List<String> list = mock(List.class);
		Mockito.when(list.get(0)).thenReturn("javatraining");
		assertEquals("javatraining", list.get(0));
		assertNull(list.get(1));
	}

	@Test
	public void letsMockListGetWithAny() {
		List<String> list = mock(List.class);
		Mockito.when(list.get(Mockito.anyInt())).thenReturn("javatraining");
		// If you are using argument matchers, all arguments
		// have to be provided by matchers.
		assertEquals("javatraining", list.get(0));
		assertEquals("javatraining", list.get(1));
	}

}




Dependency:
______________

	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>
	
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.10.19</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.8.0</version>
					<configuration>
						<release>11</release>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>3.0.0-M4</version>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
