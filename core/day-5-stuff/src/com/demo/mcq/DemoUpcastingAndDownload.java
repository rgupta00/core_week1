package com.demo.mcq;

class A {
	public void show() {
		System.out.println("show A");
	}
}

class B extends A {
	public void show() {
		System.out.println("show B");
	}

	public void show2() {
		System.out.println("show2 B");
	}
}

class C extends A {
	public void show() {
		System.out.println("show C");
	}

	public void show3() {
		System.out.println("show3 C");
	}
}

public class DemoUpcastingAndDownload {

	public static void main(String[] args) {
		// why upcasting is not a problem ?

		// A a=new B(); // upcasting
		// a.show();
		// ((B) a).show2();

//		B b=new A();// wont work
//		b.show();
//		b.show2();

//		A a1=new B();
//		
//		B b1=(B) a1;
//		if(b1 instanceof B)
//		b1.show2();

		A a1 = new B(); //we have assigned object of B to ref of A
		C c1=(C) a1;// we are downcasting the object which is ref by ref of A type to C
		//actually object is of type of B not C
		
		c1.show3();
		
		

	}

}
