package com.searching;

import java.util.Arrays;

public class BS {
	public static void main(String[] args) {
		int arr[]= {3,5,6,-3,78,22};
		Arrays.sort(arr);// first sort   //-3 3 5 6 22 78
		BS.binarySearch(arr, 0, arr.length, 6);
		
	}
	public static void binarySearch(int arr[], int first, int last, int key) {
		int mid = (first + last) / 2;
		while (first <= last) {
			if (arr[mid] < key) {
				first = mid + 1;
			} else if (arr[mid] == key) {
				System.out.println("Element is found at index: " + mid);
				break;
			} else {
				last = mid - 1;
			}
			mid = (first + last) / 2;
		}
		if (first > last) {
			System.out.println("Element is not found!");
		}
	}
}
