package com.sorting;

public class Bubble {
	public static void main(String[] args) {
		int a[]= {38,52,9,18, 6,62,13};
		int temp;
		for(int i=0;i<a.length; i++)
		{
			for(int j=0;j<a.length-1; j++) 
			{
				if(a[j]> a[j+1]) 
				{
					temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
		for(int val: a) {
			System.out.print(val+" ");
		}
	}
}
