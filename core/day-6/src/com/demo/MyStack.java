package com.demo;

public class MyStack {
	
	private char[] data;
	private int top;
	private final int SIZE=10;
	public MyStack() {
		data=new char[SIZE];
		top=-1;
	}
	public void push(char val) {
		if(top ==SIZE-1) {
			System.out.println("full");
		}else {
			data[++top]=val;
		}
	}
	
	public char pop() {
		if(isEmpty()) {
			return 'x';
		}else {
			return data[top--];
		}
	}
	public boolean isEmpty() {
		return (top==-1)? true: false;
	}
	public boolean isFull() {
		return (top ==SIZE-1) ? true: false;
	}
	
}
