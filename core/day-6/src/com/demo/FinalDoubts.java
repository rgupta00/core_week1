package com.demo;

class Book{
	private int id;
	private String price;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Book(int id, String price) {
		super();
		this.id = id;
		this.price = price;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Book [id=").append(id).append(", price=").append(price).append("]");
		return builder.toString();
	}

}
class BookApp{
	
	public void processBook(final Book book) {
		//book=null;
	}
}
public class FinalDoubts {
	
	public static void main(String[] args) {
		//final ref
		final Book book=new Book(121, "500");
		System.out.println(book);
		// IF A REF IS FINAL DONT MEAN U CANT THE CHANGE THE OBJECT
		// IN FACT U CAN CHANGE THE STATE OF THE OBJECT
		//book.setId(1277);
		//book=null;
		System.out.println(book);
	}

}
