package com.demo.exceptions;
import static java.lang.Math.*;

import java.util.Scanner;
class InvalidRadiusException extends Exception{

	public InvalidRadiusException(String message) {
		super(message);
	}
	
}
class Circle{
	private double radius;
				//throws on method signature tell that it is a risky method
	public Circle(double radius) throws InvalidRadiusException {
		if(radius<=0)
			throw new InvalidRadiusException("radius "+ radius + " is invalide");// as my business condition is failed
	
						// i will create a ex object and throw explicitly
		this.radius = radius;
	}
	
	public double getArea() {
		return PI* radius * radius;
	}
	
}
public class DemoCheckedEx {
	
	public static void main(String[] args) {
		System.out.println("PE radius");
		Scanner scanner=new Scanner(System.in);
		double radius=scanner.nextDouble();
		
		try{
			Circle circle=new Circle(radius);
			System.out.println(circle.getArea());
		}catch(InvalidRadiusException ex) {
			System.out.println(ex.getMessage());
		}
	
		
	}

}
