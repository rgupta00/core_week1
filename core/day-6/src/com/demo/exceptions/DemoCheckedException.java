package com.demo.exceptions;
import java.io.*;
import java.util.*;
public class DemoCheckedException {
	
	public static void main(String[] args)  {
		//i can create a set : dont allow duplication
		Set<String> words=new HashSet<String>();
		
		//u need to read a file
		try {
			BufferedReader br=new BufferedReader(new FileReader("story.txt"));
			
			String line=null;
			while((line=br.readLine())!=null) {
				String tokens[]=line.split(" ");
				for(String token : tokens) {
					words.add(token);
				}
				
			}
			
			
			for(String word: words) {
				System.out.println(word);
			}
			
		} 

		catch (FileNotFoundException e) {
			System.out.println("file is not found");
		}
		catch(IOException e) {
			System.out.println("io ex");
		}
		catch(Exception e) {
			System.out.println("some other ex happens");
		}
		
		
		
	}
}
