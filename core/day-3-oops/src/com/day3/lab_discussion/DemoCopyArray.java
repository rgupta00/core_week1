package com.day3.lab_discussion;

class MyArray{
	int x[];
	public MyArray() {
		x= new int[4];
		x[0]=1;
		x[1]=2;
		x[2]=12;
		x[3]=26;
		
	}
	//what is exprected : rather then retuning original array, you better return 
	//copy of that array : otherwise we may hv problem while implementing " immutable classes"
	
	public  int [] copyArray() {
		return x;
	}
	public void print() {
		for(int i=0;i<x.length; i++) {
			System.out.println(x[i]);
		}
	}
}
public class DemoCopyArray {

	public static void main(String[] args) {
		MyArray array=new MyArray();
		array.print();
		int temp[]=array.copyArray();
		System.out.println("---------");
		temp=null;
		array.print();
		
		
	}
}
