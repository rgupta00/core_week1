package com.day3.lab_discussion;

class Emp{
	private int id;
	private String name;
	private double salary;
	
	public Emp(int id, String name, double salary) {
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getSalary() {
		return salary;
	}
	
	public void show() {
		System.out.println("id: "+ id);
		System.out.println("name: "+ name);
		System.out.println("salary: "+ salary);
	}
}

class EmpApp{
	private Emp [] emps ;
	
	public double getTotalSalary() {
		double totalSalary=0;
		for(Emp emp: emps) {
			totalSalary+=emp.getSalary();
		}
		return totalSalary;
	}
	
	
}
public class PassingArrayOfEmp {

	public static void main(String[] args) {
	
		Emp[]emps=new Emp[4];
		emps[0]=new Emp(121, "raja", 40.0);
		emps[1]=new Emp(12, "amit", 40.0);
		emps[2]=new Emp(21, "TS", 40.0);
		emps[3]=new Emp(1291, "syed", 40.0);
		
//		for(int i=0; i< emps.length; i++) {
//			emps[i].show();
//		}
		
		for(Emp emp: emps) {
			emp.show();
		}
		
		//enhace for loop
//		int x[]= {4,6,7,8};
//		
//		for(int val: x) {
//			System.out.println(val);
//		}
		
		
		
//		for(int i=0;i<x.length; i++) {
//			System.out.println(x[i]);
//		}
		
		//java 1.5
		//enhane for loop
		
	
	}
}
























