package com.day3.lab_discussion;

class MatrixOperation{
	
	public static int[][] sum(int x[][], int y[][]) {
		
		int arr[][]=new int[3][3];
		for(int i=0;i< x.length; i++) {
			for(int j=0;j<x[i].length ; j++) {
				arr[i][j]=x[i][j]+y[i][j];
			}
		}
		return arr;
	}
	
	public static int[][] diff(int x[][], int y[][]) {
		int arr[][]=new int[3][3];
		for(int i=0;i< x.length; i++) {
			for(int j=0;j<x[i].length ; j++) {
				arr[i][j]=x[i][j]-y[i][j];
			}
		}
		return arr;
	}
}

public class PassingArrayDoubts {
	
	public static void main(String[] args) {
		int x[][]= {{1,2,3},{2,4,6},{2,4,5}};
		int y[][]= {{1,2,3},{2,4,6},{2,4,5}};
		
		int z[][]=MatrixOperation.diff(x, y);
		
		//enhacnce loop
		for(int temp[]: z) {
			for(int val: temp) {
				System.out.print(val+" ");
			}
			System.out.println();
		}
		
	}

}
