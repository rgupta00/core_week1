package com.day3.session2;
/*
 * java 8: u can a method imp allowed inside a interface (100% ab)
 * u can also define static method
 * 
 * What about diamond problem
 */

interface A{
	public default void foo() {
		System.out.println("foo inside interface A");
	}
}

interface B{
	public default void foo() {
		System.out.println("foo inside interface B");
	}
}


class C implements A, B{
	
	public  void foo() {
		A.super.foo();
		B.super.foo();
		System.out.println("foo is overriden");
	}
}


//interface Foof{
//	public void foo();
//	public default void foo2() {
//		System.out.println("foo2 defualt implementation....");
//	}
//	public static void fooStatic() {
//		System.out.println("foo static method");
//	}
//}
//class FoofImpl implements Foof{
//	public void foo() {
//		System.out.println("foo is impl....");
//	}
//	
//	public  void foo2() {
//		System.out.println("foo2 defualt is overriden....");
//	}
//}

public class InterfaceInJava8 {
	public static void main(String[] args) {
		
		A a=new C();
		a.foo();
		
		
//		Foof f=new FoofImpl();
//		f.foo2();
//		Foof.fooStatic();
		//Why ? 
		
	}

}
