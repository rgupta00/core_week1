package com.day3.session1;

class X1{
	int i=10;
	public void print() {
		System.out.println(this.i);
	}
}
class X2 extends X1{
	int i=100;
	public void print() {
		System.out.println(this.i);
	}
}
public class DemoOverriding {
	
	public static void main(String[] args) {
		X1 x=new X2();
		x.print();
		
		
//		Foof2 f=new Foof2();
//		f.foo();
	}

}



//class Foof{
//	
//	public static void foo() {
//		System.out.println("foo of Foof");
//	}
//}
//
//class Foof2 extends Foof{
//	//shadowing concepts
//	public static void foo() {
//		System.out.println("foo of Foof2");
//	}
//}