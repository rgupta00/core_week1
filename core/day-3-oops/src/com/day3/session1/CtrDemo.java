package com.day3.session1;
//Singleton design pattern
class Bar{
	
//	static  Bar() {
//		System.out.println("ctr cant not be static ");
//	}
}
class Foo{
	private int i;
	private static Foo foo=new Foo();
	
	private Foo() {
		System.out.println("private ctr is possible");
	}
	public static Foo getFoo() {
		return foo;
	}
	public void fooWork() {
		System.out.println(" foo work method");
	}
}
public class CtrDemo {

	public static void main(String[] args) {
		//what is the trick ?
		//for calling static method u dnot need object of that class!
		Foo foo1=Foo.getFoo();
		Foo foo2=Foo.getFoo();
		
		System.out.println(foo1.hashCode());
		System.out.println(foo2.hashCode());
	}
}



