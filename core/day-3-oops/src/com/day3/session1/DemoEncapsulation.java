package com.day3.session1;
//sir is it a good abs and encapsulation?
class Account{
	private int id;
	private String name;
	private double balance ;
	public Account() {
		
		this(2,"raj",5000.00);// super and this can not come togather
		
	}
	public Account(int id, String name, double balance) {
		System.out.println("inside account(int id, String name, double balance) ctr");
		this.id = id;
		this.name = name;
		this.balance = balance;
	}
	
	public void deposit(double amount) {
		balance=balance+ amount;
	}
	
	
	public void withdraw(double amount) {
		balance=balance- amount;
	}
	
	public double getBalance() {
		return balance;
	}
	public void printDetails() {
		System.out.println("id: "+ id);
		System.out.println("name: "+ name);
		System.out.println("balance: "+ balance);
	}

	public int getId() {
		return id;
	}
	//BL account id cant change once acc is created
//	public void setId(int id) {
//		this.id = id;
//	}

	public String getName() {
		return name;
	}

	//BL account holder name cant change once acc is created
//	public void setName(String name) {
//		this.name = name;
//	}

	//BL account balance can only be change through proper chennal : deposit name cant change once acc is created
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
}
public class DemoEncapsulation {
	
	public static void main(String[] args) {
		Account account=new Account();
		
	
		
		account.printDetails();
	}

}
