
class MyClass{
	private static MyClass class1=new MyClass();
	
	public static MyClass getMyClass() {
		return class1;
	}
	private MyClass() {
		System.out.println("dare to call me");
	}
	public void businessMethod() {
		System.out.println("call me i am a business method...");
	}
}

public class DemoPrivateCtr {
	public static void main(String[] args) {
		
		MyClass class1=MyClass.getMyClass();
		
		class1.businessMethod();
		
	}

}
