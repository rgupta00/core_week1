class Demo{
	int i=5;
	static int j=4;
}
public class DemoStatic {

	public static void main(String[] args) {
		Demo demo1=new Demo();
		Demo demo2=new Demo();
		System.out.println("-------init---------");
		System.out.println(demo1.i +" : "+ demo1.j);
		System.out.println(demo2.i +" : "+ demo2.j);
		
		System.out.println("-------change i for only one object---------");
		demo1.i=55;
		System.out.println(demo1.i +" : "+ demo1.j);
		System.out.println(demo2.i +" : "+ demo2.j);
		
		System.out.println("-------change j for both---------");
		
		demo1.j=555;
		System.out.println(demo1.i +" : "+ demo1.j);
		System.out.println(demo2.i +" : "+ demo2.j);
		
	}
}
