package com.demo;

import java.util.Arrays;

public class Demo {

	public static void main(String[] args) {
		//removing duplicate element from an array
		
		//1. Approach 1: sort the array then remove duplicate element
		int a[]= {11,1,7,11,19,21};
		int j=0;
		Arrays.sort(a);
		//1, 7, 11, 11, 19, 21 
		
		for(int i=0;i< a.length-1; i++) {
			if(a[i]!=a[i+1]) {
				a[j++]=a[i];
			}
		}
		
		//insert last element
		a[j++]=a[a.length-1];
		
		//printing
		for(int i=0;i<j; i++) {
			System.out.print(a[i]+" ");
			
		}
	
	}
	

}
