
public class LinearSearch {
	
	public static void main(String[] args) {
		int x[]= {3,5,6,7,8,90,-1};
		
		boolean isFound=false;
		int val=68;
		int pos=-1;
		//0(N)
		for(int i=0;i< x[i]; i++) {
			if(x[i]==val) {
				isFound=true;
				pos=i;
				break;
			}
		}
		if(isFound)
			System.out.println("found:"+ pos);
		else
			System.out.println("not found");
	}

}
