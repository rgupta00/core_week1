
public class Demo2DArray {

	public static void main(String[] args) {
		int x[][] = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

		print2D(x);

		// check wheter it is identity matix or not?
		
		boolean isIdetityMatix = checkIdentityMatrix(x);
		
		System.out.println(isIdetityMatix);
	}

	private static boolean checkIdentityMatrix(int[][] x) {
		boolean isIdetityMatix = true;
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[i].length; j++) {

				if (i == j) {
					if (x[i][j] != 1) {
						isIdetityMatix = false;
						break;
					}
				}
				if (i != j) {
					if (x[i][j] != 0) {
						isIdetityMatix = false;
						break;
					}

				}
			}
		
		}
		return isIdetityMatix;
	}

	private static void print2D(int[][] x) {
		for (int temp[] : x) {
			for (int val : temp) {
				System.out.print(val + " ");
			}
			System.out.println();
		}
	}

}
