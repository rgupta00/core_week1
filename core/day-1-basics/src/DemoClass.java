
class Account{
	
	// data + method 
	// instance data: per object 
	//, class data: per class ,static data
	//local data: inside the method ... not visible outside
	
	private int accountId;
	private String accountHolderName;
	private double accountBalance;
	
	
	//Account must have max 5L
	
	
	public void deposit(double amount) {
		double tempBal=this.accountBalance+amount;
		if(tempBal>50_00_00) {
			System.out.println("u cant deposit more then or eq to 5L balance....");
		}
		else
			this.accountBalance=tempBal;
			
	}
	//Account must have min 1000
	
	public void withdraw(double amount) {
		double tempBal=this.accountBalance-amount;
		if(tempBal<1000) {
			System.out.println("insufficient balance....");
		}
		else
			this.accountBalance=tempBal;
			
	}
	
	//default ctr
	public Account() {
		System.out.println("default ctr");
	}
	//para ctr
	public Account(int accountId, String accountHolderName,double accountBalance ) {
		this.accountId=accountId;
		this.accountHolderName=accountHolderName;
		this.accountBalance=accountBalance;
	}
//	//copy ctr: u want to create a object with the state of another objects
//	
//	public Account(Account account) {
//		accountId=account.accountId;
//		
//	}
	
	
	public void printAccountDetails() {
		System.out.println("Account id: "+accountId);
		System.out.println("accountHolderName: "+accountHolderName);
		System.out.println("accountBalance: "+accountBalance);
		
	}
}

public class DemoClass {
	
	public static void main(String[] args) {
		Account account=new Account(23,"raj",5000.00);// i am calling default ctr
		
	
		account.printAccountDetails();
		
		account.withdraw(700.00);
		
		System.out.println("---------");
		account.printAccountDetails();
		
	}
}







