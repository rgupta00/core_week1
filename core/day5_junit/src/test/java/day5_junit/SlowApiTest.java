package day5_junit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SlowApiTest {

	private SlowApi slowApi;
	
	@Before
	public void setUpBefore() {
		slowApi=new SlowApi();
		
	}
	
	@Test(timeout = 1500)
	public void slowApiTest() {
		double price= slowApi.api("HPLaptop");
		Assert.assertEquals(57.8, price,0.1);
		///
	}
	
	@After
	public void tearDownAfter() {
		slowApi=null;
		
		
	}
}
