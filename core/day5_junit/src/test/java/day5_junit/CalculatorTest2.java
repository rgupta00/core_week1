package day5_junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculatorTest2 {

	@BeforeClass
	public static void setUpProject() throws Exception {
		System.out.println("i am going to run only once at the start:@BeforeClass ");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("i am going to run before every test case @Before ");
	}
	

	@Test
	public void test() {
		System.out.println("test method1");
	}

	@Test
	public void test2() {
		System.out.println("test method1");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("i am going to run after every test case @After ");
	}

}
