package day5_junit;

public class SlowApi {

		public double api(String productName) {
			//it take some time to process the data
			//in order to simulate that i am create a sleep *
			try {
				Thread.sleep(1000);
			}catch(InterruptedException ex) {}
			
			return 57.8;
		}
}
