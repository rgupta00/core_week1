//package com.bookapp.service;
//
//import static org.mockito.Mockito.when;
//
//import java.util.Arrays;
//import java.util.List;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import com.bookapp.dao.BookDao;
//
//@RunWith(MockitoJUnitRunner.class)
//public class DemoTest2 {
//
//	@InjectMocks
//	BookServiceImpl bookService = new BookServiceImpl(); //even we dont need to create object
//
//	
//	@Mock
//	BookDao dao;
//	
//
//	@Before
//	public void before() {
//		List<String> allbooks = Arrays.asList("java", "rich dad poor dad", "java adv");
//
//		when(dao.getBooks()).thenReturn(allbooks);
//		
//	}
//
//	@Test
//	public void getBookTest() {
//
//		bookService.setBookDao(dao);
//		List<String> books = bookService.getBooks("java");
//		Assert.assertEquals(2, books.size());
//	}
//}
