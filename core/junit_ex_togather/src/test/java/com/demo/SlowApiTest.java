package com.demo;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SlowApiTest {
	private SlowApi slowApi;
	@Before
	public void setUp() {
		slowApi=new SlowApi();
	}
	@Test(timeout = 1000)
	public void slowApiTest() {
		
		double price= slowApi.getPrice("hp laptop");
		Assert.assertEquals(55.88, price, 0.1);
	}
	@After
	public void tearDown() {
		slowApi=null;
	}
}