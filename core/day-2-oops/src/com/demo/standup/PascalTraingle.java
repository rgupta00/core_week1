package com.demo.standup;

public class PascalTraingle {

	public static void main(String[] args) {
		int a[][]=new int[5][5];
		int i, j;
		int n=5;
		 for(i=0;i<n;i++){
	            for(j=0;j<=i;j++)
	                if(j==0 || j==i)
	                    a[i][j]=1;
	                else
	                    a[i][j]=a[i-1][j-1]+a[i-1][j];
	      }
		 
		 //printing pascal T
		 
		 for( i=0;i< a.length; i++) {
			 for( j=0;j<=i; j++) {
				 System.out.print(a[i][j]);
			 }
			 System.out.println();
		 }
		 
	}
}
