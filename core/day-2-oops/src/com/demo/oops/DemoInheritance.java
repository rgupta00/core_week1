package com.demo.oops;

class Box2D{
	private int l, b;

	public Box2D(int l, int b) {
		this.l = l;
		this.b = b;
	}
	
	public void printBox2D() {
		System.out.println("l: "+ l);
		System.out.println("b: "+ b);
	}
}

class Box3D extends Box2D{
	private int h;

	public Box3D(int l, int b, int h) {
		super(l, b);
		this.h=h;
	}
	
	public void printBox3D() {
		printBox2D();
		System.out.println("h: "+ h);
	}
}

class Box3DWithColor extends Box3D{
	private String color;

	public Box3DWithColor(int l, int b, int h, String color) {
		super(l, b, h);
		this.color=color;
	}
	
	public void printBox3DWithColor() {
		printBox3D();
		System.out.println("color: "+ color);
	}
}

public class DemoInheritance {
	
	public static void main(String[] args) {
		Box3DWithColor box3dWithColor=new Box3DWithColor(2,4,6,"red");
		box3dWithColor.printBox3DWithColor();
	}

}
