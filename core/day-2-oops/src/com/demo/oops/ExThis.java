package com.demo.oops;

class Employee{
	private int id;
	private String name;
	private double salary;
	
	private static String companyName="CGI india";
	
	public Employee() {}

	public Employee(int id, String name, double salary) {
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	//it is used to make a copy of the object with same data as original object
	public Employee(Employee emp) {
		this.id=emp.id;
		this.name=emp.name;
		this.salary=emp.salary;
	}
	
	public void print() {
		System.out.println("Id: "+ id);
		System.out.println("name: "+ name);
		System.out.println("salary: "+ salary);
	}
	//getters and setters
	// mutator : who can mutate (change ) the data: setter are mutators
	//and immutator: can not : "normally" getter are immutable

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public static String getCompanyName() {
		return companyName;
	}

	public static void setCompanyName(String companyName) {
		Employee.companyName = companyName;
	}
	
	
	public static void printCompanyName() {
		//INSIDE STATIC METHOD THIS POINTER IS NOT ACCESSABLE
		// ie u cant get instance variable inside static method
		
		System.out.println("company name: "+ companyName);
	}
}
public class ExThis {

	public static void main(String[] args) {

		//compiler will do the code optimization 
		
		Employee employee=null;
		employee.printCompanyName();// Employee.printCompanyName();
		
		
//		//for calling static method we dont need object of that class (imp)
//		
//		Employee.setCompanyName("CGI india");
//		Employee employee=new Employee(12, "raj", 5000);
//		Employee employee2=new Employee(52, "ekta", 5000);
//
//
//		employee.print();
//		employee2.print();
//		
//		System.out.println(Employee.getCompanyName());
		
	}
}
