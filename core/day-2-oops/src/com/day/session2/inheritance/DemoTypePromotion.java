package com.day.session2.inheritance;
class A{
	void foo() {
		System.out.println("foo method of A class");
	}
}

class B extends A{
	//Overriding?
	void foo() {
		System.out.println("foo method of B class");
	}
}

//class C extends A{
//	void foo() {
//		System.out.println("foo method of A class");
//	}
//}


public class DemoTypePromotion {
	
	public static void main(String[] args) {
		//ref of base class= object of drived class
		//this is called run time polymorohism: ie 
		//which method is going to be called is not decided at compile time
		//it would be decied at  run time
		
		
		A a=new B();
		
		a.foo();
		
//		double i=44.77;
//		int j=(int)i;
//		System.out.println(j);
		
	}

}
