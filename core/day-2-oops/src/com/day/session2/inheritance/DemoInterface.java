package com.day.session2.inheritance;

interface A1{
	int i=100;
	void foo();// public abst
}
 class A1Impl implements A1{
	 //while doing overriding u can only increse visibility u cant decrease it! => Why?
	public void foo() {
		System.out.println(i);
		System.out.println("foo of A1Impl1");
	}
}
public class DemoInterface {
	
	public static void main(String[] args) {
		A1 a1=new A1Impl();
		a1.foo();
	}

}
