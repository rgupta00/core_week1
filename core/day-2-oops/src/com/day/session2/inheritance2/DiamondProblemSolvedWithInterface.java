package com.day.session2.inheritance2;

class X{
	 void foo() {
		 System.out.println("foo of X");
	 }
}

class Y{ 
	 void foo() {
		System.out.println("foo of Y"); 
	 }
}

class Z extends X, Y{
	
	@Override
	public void foo() {
		super.foo();
		System.out.println("z foo method");
	}
}

public class DiamondProblemSolvedWithInterface {
	
	public static void main(String[] args) {
		
	}

}
