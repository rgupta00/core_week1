package com.day.session2.travel;

/*
 * Stack: LIFO
 *  Queue: FIFO
 */
interface Stack{
	public void push(int i);
	public int pop();
}
class StackNeeraj implements Stack {
	private int x[];
	private final int SIZE = 10;

	private int top;

	public StackNeeraj() {
		x = new int[SIZE];
		top = -1; // there is nothing in stack
	}

	public void push(int data) {
		if (!isFull()) {
			x[++top] = data;
		} else
			System.out.println("overflow...");
	}

	boolean isFull() {
		return top == SIZE - 1 ? true : false;
	}

	public int pop() {
		if (!isEmpty()) {
			return x[top--];
		} else
			return -999;
	}

	boolean isEmpty() {
		return top == -1 ? true : false;
	}
}


class StackTs implements Stack{
	private int x[];
	private final int SIZE = 10;

	private int top;

	public StackTs() {
		x = new int[SIZE];
		top = -1; // there is nothing in stack
	}

	public void push(int data) {
		if (!isFull()) {
			x[++top] = data;
		} else
			System.out.println("overflow...");
	}

	boolean isFull() {
		return top == SIZE - 1 ? true : false;
	}

	public int pop() {
		if (!isEmpty()) {
			return x[top--];
		} else
			return -999;
	}

	boolean isEmpty() {
		return top == -1 ? true : false;
	}
}

public class StackExample {

	public static void main(String[] args) {
		//design as per interface
		
		Stack stack=new StackNeeraj();
		
		stack.push(12);
		stack.push(182);
		stack.push(120);
		stack.push(902);
		
		
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		
		System.out.println(stack.pop());
		
		
		
	}

}
