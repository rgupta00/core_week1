package com.day.session2.travel;
//A  passanger is traveling from BTM  to LalBagh using metro
//
interface Vehical{
	public void move(String s, String d) ;
}
class Bike implements Vehical{
	public void move(String s, String d) {
		System.out.println("moving on bike from BTM to lalbagh");
	}
}
class Metro implements Vehical{
	public void move(String s, String d) {
		System.out.println("moving in metro from BTM to lalbagh");
	}
}
class Bus implements Vehical{
	public void move(String s, String d) {
		System.out.println("moving in bus from BTM to lalbagh");
	}
}
class Passanger{
	private String name;
	public Passanger(String name) {
		this.name=name;
	}
	
	public void travel(String s, String d, Vehical vehical) {
		System.out.println("passanger name: "+ name);
		vehical.move(s, d);// delegation of resp...
	}
	
}
public class TransportProblem {
	
	public static void main(String[] args) {
		Vehical vehical=new Bike();
		
		
		Passanger passanger=new Passanger("raj");
		passanger.travel("BTM", "LB", vehical);
		
	}

}
